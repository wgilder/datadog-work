from checks import AgentCheck
import random

class RandomCheck(AgentCheck):
  def check(self, instance):
    value = random.randint(0,1000)
    self.gauge('my_metric', value)

