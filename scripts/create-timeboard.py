from datadog import initialize, api
import time
import datetime
import sys

DATADOG_TEST_DB_LIST_NAME = "DataDogTestList"
DATADOG_TEST_TIMEBOARD_NAME = "DataDogTestTimeboard"

options = {
    'api_key': sys.argv[1],
    'app_key': sys.argv[2]
}


initialize(**options)

def get_all_db_lists():
    result = api.DashboardList.get_all()
    return result

def print_db_list(id):
    result = api.DashboardList.get(id)
    print(result)

def get_test_db_list(force = False):
    lists = get_all_db_lists()
    lists = lists['dashboard_lists']
    id = -1
    
    for l in lists:
        if l['name'] == DATADOG_TEST_DB_LIST_NAME:
            id = l['id']
            break

    if force and id >= 0:
        api.DashboardList.delete(id)
        id = -1

    if id < 0:
        l = api.DashboardList.create(name=DATADOG_TEST_DB_LIST_NAME)
        id = l['id']

    return id
    
def create_db_timeboard():
    graphs = [{
        "title": "my_metric on Sox",
        "definition": {
            "viz": "timeseries",
            "requests": [
                {"q": "avg:my_metric{host:sox.cafebabe.us}",}
            ]
        },
    },{
        "title": "PostreSQL Connections Usage (%)",
        "definition": {
            "viz": "timeseries",
            "requests": [
                {"q": "anomalies(avg:postgresql.percent_usage_connections{host:sox.cafebabe.us}, 'basic', 2)",}
            ]
        },
    },{
        "title": "my_metric hourly rollup average",
        "definition": {
            "viz": "timeseries",
            "requests": [
                {"q": "avg:my_metric{host:sox.cafebabe.us}.rollup(avg, 3600)",}
            ]
        },
    }]

    ts = datetime.datetime.today().strftime("%m%d%H%M%S")
    name = "{}-{}".format(DATADOG_TEST_TIMEBOARD_NAME, ts)
    desc = "Timeboard for showing my_metric information for host sox.cafebabe.us"
    read_only = False
    result = api.Timeboard.create(title=name, description=desc, graphs=graphs, read_only=read_only)
    print (result)

create_db_timeboard()